﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SquareEquationSolving;

public class Solver : ISolve
{
    public double[] Solve(Double a, Double b, Double c)
    {
        if (IsValueEqualToZero(a)) 
            throw new ArgumentException("Coefficient A mast not be equal to zero");

        if (IsValueTooLarge(a)|| IsValueTooLarge(b)|| IsValueTooLarge(c)) 
            throw new ArgumentException("Coefficient is too large");

        Double cast_b = b / a;
        Double cast_c = c / a;

        Double d;
        d = cast_b * cast_b - 4 * cast_c;
        if (d < 0) return new double[0] {};
        //Double x1 = (-b + Math.Sqrt(d)) / 2;
        //Double x2 = (-b - Math.Sqrt(d)) / 2;
        Double x1 = -(cast_b + Math.Sign(cast_b) * d) / 2;
        Double x2 = cast_c / x1;
        return new double[2] { x1, x2 };
    }

    internal bool IsValueEqualToZero(Double value) 
    {
        return Math.Abs(value) < Double.Epsilon;
    }

    internal bool IsValueTooLarge(Double value)
    {
        return Math.Abs(value) >= Double.MaxValue;
    }

   

}
