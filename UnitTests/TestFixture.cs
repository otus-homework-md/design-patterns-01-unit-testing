﻿using SquareEquationSolving;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests
{
    public class TestFixture : IDisposable
    {
        public ISolve Solver { get; private set; }
        public TestFixture() 
        {
            Solver = new Solver();
        }
        public void Dispose()
        {
        }
    }
}
