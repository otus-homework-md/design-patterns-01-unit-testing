﻿using FluentAssertions;
using SquareEquationSolving;
using Xunit;

namespace UnitTests;


public class SolverTest : IClassFixture<TestFixture>
{
    private readonly ISolve _solver;

    public SolverTest(TestFixture testFixture)
    {
        _solver = testFixture.Solver;
    }

    [Fact]
    public void Solve_WithAEqualToZero_ShouldThoroughException()
    {
        //Assign 
        // take a _solve class from fixture


        //Act and Assert
        Assert.Throws<ArgumentException>(() => _solver.Solve(0,1,1));
    }

    [Fact]
    public void Solve_WithAEqualToOneBEqualToZeroCEqualtoOne_ShouldNotHaveAnyRoots()
    {
        //Assign 
        // take a _solve class from fixture

        //Act 
        Double[] roots = _solver.Solve(1, 0, 1);

        var numberOfRoots = roots.Length;

        // Assert
        numberOfRoots.Should().Be(0); 
    }

    [Fact]
    public void Solve_WithAEqualToOneBEqualToZeroCEqualtoMinusOne_ShouldHaveTwoRootMultiplicityOne()
    {
        //Assign 
        // take a _solve class from fixture

        //Act 
        Double[] roots = _solver.Solve(1, 0, -1);

        var numberOfRoots = roots.Length;

        // Assert
        numberOfRoots.Should().Be(2);
        roots[0].Should().NotBe(roots[1]);
    }

    [Fact]
    public void Solve_WithAEqualToOneBEqualToTwoCEqualtoOne_ShouldHaveTwoEqualRoots()
    {
        //Assign 
        // take a _solve class from fixture

        //Act 
        Double[] roots = _solver.Solve(1, 2, 1);

        var numberOfRoots = roots.Length;

        // Assert
        numberOfRoots.Should().Be(2);
        roots[0].Should().Be(roots[1]);
    }

    [Fact]
    public void Solve_InputParams_ShouldBeNumbers()
    {
        //Assign 
        // take a _solve class from fixture
        var a = Double.MaxValue+1;

        //Act and Assert
        Assert.Throws<ArgumentException>(() => _solver.Solve(a, 1, 1));
    }

}